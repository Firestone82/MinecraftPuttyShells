#!/bin/bash

# Colors
COLOR_REST="$(tput sgr0)"
COLOR_RED="\e[91m"
COLOR_GREEN="\e[92m"
COLOR_YELLOW="\e[93m"

# Name	 Path				    RAM MAX_RAM
# -------------------------------------------
# server /home/servers/survival 2G  1G
#
# Config path
dos2unix /home/autorun/autostart/config.cfg 2> /dev/null
config="/home/autorun/autostart/config.cfg"

# Java arguments
# args="-XX:+UseG1GC Space -XX:+UnlockExperimentalVMOptions"
args="-XX:+UseG1GC Space -XX:+ParallelRefProcEnabled Space -XX:MaxGCPauseMillis=200 Space -XX:+UnlockExperimentalVMOptions Space -XX:+DisableExplicitGC Space -XX:+AlwaysPreTouch Space -XX:G1NewSizePercent=30 Space -XX:G1MaxNewSizePercent=40 Space -XX:G1HeapRegionSize=8M Space -XX:G1ReservePercent=20 Space -XX:G1HeapWastePercent=5 Space -XX:G1MixedGCCountTarget=4 Space -XX:InitiatingHeapOccupancyPercent=15 Space -XX:G1MixedGCLiveThresholdPercent=90 Space -XX:G1RSetUpdatingPauseTimePercent=5 Space -XX:SurvivorRatio=32 Space -XX:+PerfDisableSharedMem Space -XX:MaxTenuringThreshold=1 Space -Dusing.aikars.flags=https://mcflags.emc.gs Space -Daikars.new.flags=true"

# Bungee arguments
# args="-XX:+UseG1GC Space -XX:+UnlockExperimentalVMOptions"
bungee_args=""

start() {
	# Variables from path
	server="$(cat $config | grep -m1 $1 | awk '{print$1}')"
	path="$(cat $config | grep -m1 $1 | awk '{print$2}')"
	ram="$(cat $config | grep -m1 $1 | awk '{print$3}')"
	max_ram="$(cat $config | grep -m1 $1 | awk '{print$4}')"
	count="$(ps -x | grep -c 'jar '$1'.jar')"

	echo -e "Server $server"

	# Test if server is already running
	if [ "$count" = "2" ] ; then
		echo -e "${COLOR_REST} - ${COLOR_RED}Error! Server is already running! ${COLOR_REST}"
	else 
		# Test if tmux session is created
		if ! tmux has-session -t $1 2> /dev/null; then 
			# Create tmux session
			tmux new-session -d -s $1
		fi

		# Test if tmux session was created or was successfully created
		if tmux has-session -t $1 2> /dev/null; then
			echo -e "${COLOR_REST} - ${COLOR_GREEN}Starting server ${COLOR_REST}"

			# Enter server directory
			tmux send-keys -t $1 'cd '$path C-m

			# Test if user wants to enable autostart
			if [ "$2" = "-a" ] ; then
				echo -e "${COLOR_REST} - ${COLOR_GREEN}Enabling autostart ${COLOR_REST}"

				# Create autostart file
				tmux send-keys -t $1 'touch .autostart' C-m	
			elif [ "$3" = "-a" ] ; then
				echo -e "${COLOR_REST} - ${COLOR_GREEN}Enabling autostart ${COLOR_REST}"

				# Create autostart file
				tmux send-keys -t $1 'touch .autostart' C-m	
			fi

			if [ "$1" = "bungeeUpdate" ] ; then 
				# Run java commands
				tmux send-keys -t $1 'java -Xms'$ram' -Xmx'$max_ram' '$bungee_args' -jar '$1'.jar' C-m

				# Test if server was successfully started
				if [ "$count" = "2" ] ; then
					echo -e "${COLOR_REST} - ${COLOR_GREEN}Server was successfully started ${COLOR_REST}"
				else
					echo -e "${COLOR_REST} - ${COLOR_RED}There was and error while server tried to start! ${COLOR_REST}"
				fi
			else
				# Run java commands
				tmux send-keys -t $1 'java -Xms'$ram' -Xmx'$max_ram' '$args' -jar '$1'.jar' C-m

				count="$(ps -x | grep -c 'jar '$1'.jar')"
				sleep 1
				count="$(ps -x | grep -c 'jar '$1'.jar')"

				# Test if server was successfully started
				if [ "$count" = "2" ] ; then
					echo -e "${COLOR_REST} - ${COLOR_GREEN}Server was successfully started ${COLOR_REST}"
				else
					echo -e "${COLOR_REST} - ${COLOR_RED}There was and error while server tried to start! ${COLOR_REST}"
				fi
			fi
		fi

		# Test if user wants to enter console upon start
		if [ "$2" = "-o" ] ; then
			tmux a -t $1
		elif [ "$3" = "-o" ] ; then
			tmux a -t $1
		fi
	fi

}

# Test if config exists
if [ -f "$config" ] ; then
	# Test if script has server
	if [ -z "$1" ] ; then
		echo -e "${COLOR_RED}Error, you have to specify server! ${COLOR_REST}"
	else
		if cat $config | awk '{print$1}' | grep -qwm1 $1 ; then
			start $1 $2 $3 $4
		else
			echo -e "${COLOR_RED}Error, that server isn't set in config file! ${COLOR_REST}"
		fi
	fi
else
	echo -e "${COLOR_RED}Error, can't find config file! ${COLOR_REST}"
fi
