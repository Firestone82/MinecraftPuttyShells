#!/bin/bash

# Colors
COLOR_REST="$(tput sgr0)"
COLOR_RED="\e[91m"
COLOR_GREEN="\e[92m"
COLOR_YELLOW="\e[93m"

# Name	 Path				    RAM MAX_RAM
# -------------------------------------------
# server /home/servers/survival 2G  1G
#
# Config path
config="/home/autorun/autostart/config.cfg"

# Script path
skripts="/home/autorun/autostart/"

restart() {

	# Variables
	count="$(ps -x | grep -c 'jar '$1'.jar')"

	# Test if server is already running
	if [ "$count" = "2" ] ; then
		# Stop server
		bash $skripts/stop.sh $1
		
		echo -e ""
		
		# Run restart sequence
		for i in 5 4 3 2 1
		do
			echo -e "${COLOR_YELLOW} Restart in .. $i"
			sleep 1
		done
		
		echo -e "${COLOR_REST}"

		# Start server
		bash $skripts/start.sh $1 $2 $3 $4
	else 
		echo -e "${COLOR_REST} - ${COLOR_RED}Error! This server isn't running! ${COLOR_REST}"
	fi

}

# Test if config exists
if [ -f "$config" ] ; then
	# Test if script has server
	if [ -z "$1" ] ; then
		echo -e "${COLOR_RED}}Error, you have to specify server! ${COLOR_REST}"
	else
		if cat $config | awk '{print$1}' | grep -qwm1 $1 ; then
			restart $1 $2 $3 $4
		else
			echo -e "${COLOR_RED}Error, that server isn't set in config file! ${COLOR_REST}"
		fi
	fi
else
	echo -e "${COLOR_RED}Error, can't find config file! ${COLOR_REST}"
fi

