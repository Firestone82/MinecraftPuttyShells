#!/bin/bash

# Colors
COLOR_REST="$(tput sgr0)"
COLOR_RED="\e[91m"
COLOR_GREEN="\e[92m"
COLOR_YELLOW="\e[93m"
COLOR_WHITE="\e[97m"
COLOR_DARK_GRAY="\e[97m"

# Name	 Path				    RAM MAX_RAM
# -------------------------------------------
# server /home/servers/survival 2G  1G
#
# Config path
config="/home/autorun/autostart/config.cfg"

status() {

	# Variables from path
	server="$(cat $config | grep -m1 $1 | awk '{print$1}')"
	path="$(cat $config | grep -m1 $1 | awk '{print$2}')"
	ram="$(cat $config | grep -m1 $1 | awk '{print$3}')"
	max_ram="$(cat $config | grep -m1 $1 | awk '{print$4}')"
	count="$(ps -x | grep -c 'jar '$1'.jar')"

	echo -n -e "# ${COLOR_YELLOW}$server"
		
	for (( c=0; c<=$2-${#server}; c++ ))
	do
		echo -n " "
	done

	# Test if server isn't running
	if [ "$count" = "2" ] ; then
		echo -n -e " ${COLOR_DARK_GRAY}| ${COLOR_GREEN}Running ${COLOR_REST}"
	else 
		echo -n -e " ${COLOR_DARK_GRAY}| ${COLOR_RED}Stopped ${COLOR_REST}"
	fi

	# Test if server has autostart enabled
	if [ -f $path/.autostart ]; then
		echo -n -e "${COLOR_DARK_GRAY}| ${COLOR_GREEN}Enabled ${COLOR_REST}"
	else
		echo -n -e "${COLOR_DARK_GRAY}| ${COLOR_RED}Disabled ${COLOR_REST}"
	fi

	# Print Min. RAM parameter
	echo -n -e "${COLOR_DARK_GRAY}| ${COLOR_YELLOW}$ram ${COLOR_REST}"

	word="$ram"
	for (( c=0; c<$lenght_mram-${#word}; c++ ))
	do
		echo -n " "
	done

	# Print Max. RAM parameter
	echo -n -e "${COLOR_DARK_GRAY}| ${COLOR_YELLOW}$max_ram ${COLOR_REST}"

	echo ""
}

lenght="0"

# Get servers into array
servers=$(cat $config | awk '{print$1}')

# Find out, longest server name
for srv in ${servers[@]}
do
	server="$(cat $config | grep -m1 $srv | awk '{print$1}')"

	if [ ${#server} -gt $lenght ] ; then
		lenght=${#server}
	fi
	
done

# Get servers ram into array
servers_mram=$(cat $config | awk '{print$3}')

# Find out, longest server name
lenght_mram=0
for srv in ${servers_mram[@]}
do
	server="$(cat $config | grep -m1 $srv | awk '{print$3}')"

	if [ ${#server} -gt $lenght_mram ] ; then
		lenght_mram=${#server}
	fi
done

# Get servers max ram into array
servers_maxram=$(cat $config | awk '{print$4}')

# Find out, longest server name
lenght_maxram=0
for srv in ${servers_maxram[@]}
do
	server="$(cat $config | grep -m1 $srv | awk '{print$4}')"

	if [ ${#server} -gt $lenght_maxram ] ; then
		lenght_maxram=${#server}
	fi
done

echo "#######################################################################"
echo "#"

echo -n -e "# ${COLOR_WHITE}Server"
word="Server"
for (( c=0; c<=$lenght-${#word}+1; c++ ))
do
	echo -n " "
done

echo -n -e "| ${COLOR_WHITE}Status  "
echo -n -e "| ${COLOR_WHITE}AutoStart "

echo -n -e "| ${COLOR_WHITE}Xms "
word="Xms"
for (( c=0; c<$lenght_mram-${#word}; c++ ))
do
	echo -n " "
done

echo -n -e "| ${COLOR_WHITE}Xmx "
word="Xmx"
for (( c=0; c<$lenght_maxram-${#word}; c++ ))
do
	echo -n " "
done

echo ""
echo -e "# ${COLOR_WHITE}---------------------------------------------------------------------${COLOR_REST}"

# For each server run test
for srv in ${servers[@]}
do
	status $srv $lenght
done

echo "#"
echo "#######################################################################"