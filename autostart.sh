#!/bin/bash

# Colors
COLOR_REST="$(tput sgr0)"
COLOR_RED="\e[91m"
COLOR_GREEN="\e[92m"
COLOR_YELLOW="\e[93m"

# Name	 Path				    RAM MAX_RAM
# -------------------------------------------
# server /home/servers/survival 2G  1G
#
# Config path
config="/home/autorun/autostart/config.cfg"

# Skripts path
skripts="/home/autorun/autostart/"

test_srv() {

	# Variables from path
	server="$(cat $config | grep -m1 $1 | awk '{print$1}')"
	path="$(cat $config | grep -m1 $1 | awk '{print$2}')"
	ram="$(cat $config | grep -m1 $1 | awk '{print$3}')"
	max_ram="$(cat $config | grep -m1 $1 | awk '{print$4}')"
	count="$(ps -x | grep -c 'jar '$1'.jar')"

	# Test if server has autostart enabled
	if [ -f $path/.autostart ] ; then
		# Test if server isn't running
		if [ "$count" = "1" ] ; then
			# If server isnt running and has tmux
			#if tmux has-session -t $1 2> /dev/null ; then
				# Kill tmux session
				#tmux send-keys -t $1 'exit' C-m
				#echo -e "Server $server is not running"
				#echo -e "${COLOR_REST} - ${COLOR_RED}Destroying server window ${COLOR_REST}"
			#fi

			bash $skripts/start.sh $1

		else
			echo -e "Server $1 is running"
		fi
	fi

}

# Test if config exists
if [ -f "$config" ] ; then
	while true
	do
		# Get servers into array
		servers=$(cat $config | awk '{print$1}')
		clear

		# For each server run test
		for srv in ${servers[@]}
		do
			test_srv $srv
		done

		sleep 1
	done
else
	echo -e "${COLOR_RED}Error, can't find config file! ${COLOR_REST}"
fi