<h3>Minecraft Putty Shells</h3>
<p>Allow you simply control your server without accessing your screen. Simply use command from anywhere in putty.</p>

---

## :eyeglasses: Example
<p align="center">
  <a href="https://imgur.com/a/1yA1crw">
    <img src="https://i.imgur.com/B3cfmn4.png" alt="Example1">
    <br>For more info, click me  
  </a>
</p>

---

## :pencil: Explains

1. Start Command <br>
• Simply start your server with typing "start <name_of_your_server_from_config> <br>
• If you want to enable autostart, you have to add "-a" after server name <br>
• To open console when server is starting add "-o" after <br>
 
2. Stop Command <br>
• Stop your server, script will wait until server is stopped then you will recieve message. <br>
 
3. Restart Command <br>
• Stop and Start server like nothing. <br>
• You can add flags from Start command > "-a" "-o" <br>

4. Status Command <br>
• Simply prints info about your server and configuration you have set. <br>
 
5. Autostart <br>
• Let it run in background, it will handle your servers if they crash :) <br>
• Work only if server has enabled autostart <br>
 
6. Open Command
• Let you open server console without having to type long commands
 
7. Last and important thing is Config <br>
• Make config file where you setup every server. <br>
• <img src="https://i.imgur.com/JnC8Zbh.png" alt="Setup"><br>
• <name_of_server> <path_to_server> <min_java_memory> <max_java_memory> <br>

---

## :memo: Requirements
• Tmux <br>
• Minecraft Server <br>
• Linux > Ubuntu/Debian/Other.. <br>

---

## :wrench: Installation
1. Download script into your folder.
2. Add permissions to execute that script. ``` chmod 755 <file> ```
3. Make aliases in your server ``` nano /root/.bash_aliases ```
-> <img src="https://i.imgur.com/wVtKDQQ.png" alt="Aliases">
4. Refresh your putty (Close and Re-Open it)
5. Everything should work

---

## :thumbsup: Donations

If this scripts made your life easier, you can donate to me, *so I can make more and better skripts*.
Link to <a href="https://paypal.me/firestone82?locale.x=cs_CZ">paypal</a>
