#!/bin/bash

# Colors
COLOR_REST="$(tput sgr0)"
COLOR_RED="\e[91m"
COLOR_GREEN="\e[92m"
COLOR_YELLOW="\e[93m"

# Name	 Path				    RAM MAX_RAM
# -------------------------------------------
# server /home/servers/survival 2G  1G
#
# Config path
config="/home/autorun/autostart/config.cfg"

stop() {

	# Variables from path
	server="$(cat $config | grep -m1 $1 | awk '{print$1}')"
	path="$(cat $config | grep -m1 $1 | awk '{print$2}')"
	ram="$(cat $config | grep -m1 $1 | awk '{print$3}')"
	max_ram="$(cat $config | grep -m1 $1 | awk '{print$4}')"
	count="$(ps -x | grep -c 'jar '$1'.jar')"

	echo -e "Server $server"

	# Test if server is already running
	if [ "$count" = "2" ] ; then
		# Test if server is running in tmux
		if tmux has-session -t $1 2> /dev/null ; then 
			echo -e "${COLOR_REST} - ${COLOR_RED}Stopping server ${COLOR_REST}"

			echo -e "${COLOR_REST} - ${COLOR_RED}Disabling autostart ${COLOR_REST}"
			# Create autostart file
			rm -rf $path/.autostart
			
			if [ $1 = "bungeeUpdate" ] ; then
				tmux send-keys -t $1 'e'
				tmux send-keys -t $1 'n'
				tmux send-keys -t $1 'd' C-m
			else
				# Send stop command
				tmux send-keys -t $1 'stop' C-m
			fi
			
			# Wait until server has still tmux
			while tmux has-session -t $1 2> /dev/null
			do
				count="$(ps -x | grep -c 'jar '$1'.jar')"

				# Test if server is not running
				if [ "$count" = "1" ] ; then
					# Kill tmux session
					tmux kill-session -t $1
				fi
			done

			echo -e "${COLOR_REST} - ${COLOR_RED}Server has been successfully stopped ${COLOR_REST}"
		else
			echo -e "${COLOR_REST} - ${COLOR_RED}Error! Server isn't running! ${COLOR_REST}"
		fi
	else
		echo -e "${COLOR_REST} - ${COLOR_RED}Error! Server isn't running! ${COLOR_REST}"
	fi

}

# Test if config exists
if [ -f "$config" ] ; then
	# Test if script has server
	if [ -z "$1" ] ; then
		echo -e "${COLOR_RED}Error, you have to specify server! ${COLOR_REST}"
	else
		if cat $config | awk '{print$1}' | grep -qwm1 $1 ; then
			stop $1 $2 $3 $4
		else
			echo -e "${COLOR_RED}Error, that server isn't set in config file! ${COLOR_REST}"
		fi
	fi
else
	echo -e "${COLOR_RED}Error, can't find config file! ${COLOR_REST}"
fi
