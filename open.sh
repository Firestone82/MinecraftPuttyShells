#!/bin/bash

# Colors
COLOR_REST="$(tput sgr0)"
COLOR_RED="\e[91m"
COLOR_GREEN="\e[92m"
COLOR_YELLOW="\e[93m"

# Name	 Path				    RAM MAX_RAM
# -------------------------------------------
# server /home/servers/survival 2G  1G
#
# Config path
config="/home/autorun/autostart/config.cfg"

open() {

	# Test if server console can be opened
	if tmux has-session -t $1 2> /dev/null; then 
		tmux a -t $1
	else
		echo -e "${COLOR_REST} - ${COLOR_RED}Error! This server isn't running! ${COLOR_REST}"
	fi

}

# Test if config exists
if [ -f "$config" ] ; then
	# Test if script has server
	if [ -z "$1" ] ; then
		echo -e "${COLOR_RED}Error, you have to specify server! ${COLOR_REST}"
	else
		if cat $config | awk '{print$1}' | grep -qwm1 $1 ; then
			open $1
		else
			echo -e "${COLOR_RED}Error, that server isn't set in config file! ${COLOR_REST}"
		fi
	fi
else
	echo -e "${COLOR_RED}Error, can't find config file! ${COLOR_REST}"
fi

